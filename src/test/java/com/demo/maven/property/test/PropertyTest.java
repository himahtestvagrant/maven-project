package com.demo.maven.property.test;

import com.demo.maven.property.WebBrowser;
import com.demo.maven.property.utility.BrowserUtility;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class PropertyTest {
    WebBrowser b = BrowserUtility.getBrowser();
    @Test
    public void loadBrowser(){
        WebDriver driver= b.getDriver();
        driver.get("https://www.google.com/");
        driver.quit();

    }
}
