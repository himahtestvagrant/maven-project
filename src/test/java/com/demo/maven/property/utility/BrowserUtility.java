package com.demo.maven.property.utility;
import com.demo.maven.property.FireFoxBrowser;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.demo.maven.property.ChromeBrowser;
import com.demo.maven.property.WebBrowser;

public class BrowserUtility {
    private static String browser=System.getProperty("browser");
    public static WebBrowser getBrowser() {
        if (browser.equalsIgnoreCase("chrome")) {
            ChromeBrowser cb = new ChromeBrowser();
            return cb;
        }
        else if(browser.equalsIgnoreCase("firefox")){
            FireFoxBrowser fb = new FireFoxBrowser();
            return fb;

        }
        else {
            throw new IllegalArgumentException();
        }


    }
}
