package com.demo.maven.profile.test;

import com.demo.maven.profile.utility.PropertyUtil;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.Properties;

public class ProfileTest {
    @Test
    public void load() {
        Properties prop = PropertyUtil.loadEnvProperties();
        WebDriver driver;
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        System.out.println(prop.getProperty("environment"));
        driver.get(prop.getProperty("url"));
        driver.quit();

    }
}

