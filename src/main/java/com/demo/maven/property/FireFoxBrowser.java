package com.demo.maven.property;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FireFoxBrowser extends WebBrowser{
    public FireFoxBrowser(){

        WebDriver driver;
        WebDriverManager.firefoxdriver().setup();
        driver=new FirefoxDriver();
        setDriver(driver);
    }
}
