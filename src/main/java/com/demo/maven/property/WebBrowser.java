package com.demo.maven.property;

import lombok.Data;
import org.openqa.selenium.WebDriver;
@Data


public class WebBrowser {
    private WebDriver driver;
}
